Name: libosinfo
Version: 1.8.0
Release: 1
Summary: A library for managing OS information for virtualization
License: LGPLv2+
URL: https://libosinfo.org/
Source: https://releases.pagure.org/%{name}/%{name}-%{version}.tar.xz

BuildRequires: hwdata gobject-introspection-devel osinfo-db meson cmake libsoup-devel gtk-doc
BuildRequires: libcurl-devel intltool glib2-devel
BuildRequires: perl-podlators vala vala-tools
BuildRequires: libxml2-devel >= 2.6.0
BuildRequires: libxslt-devel >= 1.0.0
Requires: hwdata osinfo-db-tools
Requires: osinfo-db >= 20180920-1

Patch0001: 0001-db-Force-anchored-patterns-when-matching-regex.patch
Patch6002: fix-build-error-for-CVE-2019-13313.patch


Provides: %{name}-vala
Obsoletes: %{name}-vala

%description
Libosinfo is designed  to provide a single place containing all the
information about an operating system that is required in order to
provision and manage it in a virtualized environment.

%package devel
Summary: Development package for libosinfo
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel pkgconfig

%description devel
The development package for libosinfo.

%package help
Summary: Help files for libosinfo

%description help
The Help files for libosindo.

%package lang
Summary: Language support for libosinfo

%description lang
Language support for libosindo.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson \
 -Denble-gtk-doc=true \
 -Denble-tests=true \
 -Denble-introspection=enabled \
 -Denble-vala=enabled
%meson_build

%install
rm -rf %{buildroot}
%meson_install
%find_lang %{name}

%check
%meson_test

%ldconfig_scriptlets

%files
%{_bindir}/osinfo-detect
%{_bindir}/osinfo-query
%{_bindir}/osinfo-install-script
%{_libdir}/%{name}-1.0.so.*
%{_libdir}/girepository-1.0/Libosinfo-1.0.typelib
%{_datadir}/vala/vapi/libosinfo-1.0.vapi
%{_datadir}/vala/vapi/libosinfo-1.0.deps
%doc AUTHORS ChangeLog COPYING.LIB NEWS README
%exclude %{_libdir}/*.la

%files devel
%dir %{_includedir}/%{name}-1.0/
%dir %{_includedir}/%{name}-1.0/osinfo/
%{_includedir}/%{name}-1.0/osinfo/*.h
%{_libdir}/%{name}-1.0.so
%{_libdir}/pkgconfig/%{name}-1.0.pc
%{_datadir}/gir-1.0/Libosinfo-1.0.gir
%{_datadir}/gtk-doc/html/Libosinfo

%files help
%{_mandir}/man1/osinfo-detect.1*
%{_mandir}/man1/osinfo-query.1*
%{_mandir}/man1/osinfo-install-script.1*

%files lang -f %{name}.lang

%changelog

* Tue Feb 2 2021 liudabo <liudabo1@huawei.com> - 1.8.0-1
- upgrade version to 1.8.0

* Sat Sep 5 2020 shixuantong <shixuantong@huawei.com> - 1.2.0-10
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: update Source0

* Wed Aug 21 2019 fangyufa <fangyufa1@huawei.com> - 1.2.0-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify info of patch

* Fri Aug 02 2019 liujing<liujing144@huawei.com> - 1.2.0-8
- Type:cves
- ID:CVE-2019-13313
- SUG:restart
- DESC:fix CVE-2019-13313

* Wed Jul 31 2019 zhuguodong <zhuguodong7@huawei.com> - 1.2.0-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: openEuler Debranding

* Sat Apr 6 2019 luochunsheng<luochunsheng@huawei.com> - 1.2.0-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove sensitive information

* Thu Jan 24 2019 wangxiao <wangxiao65@huawei.com> - 1.2.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:sync patch

* Fri Sep 6 2018 openEuler Buildteam <buildteam@openeuler.org> - 1.2.0-4
- Package init
